%global _xinputconf %{_sysconfdir}/X11/xinit/xinput.d/fcitx.conf
%{!?gtk2_binary_version: %global gtk2_binary_version %(pkg-config  --variable=gtk_binary_version gtk+-2.0)}
%{!?gtk3_binary_version: %global gtk3_binary_version %(pkg-config  --variable=gtk_binary_version gtk+-3.0)}

Name:			fcitx
Summary:		An input method framework
Version:		4.2.9.7
Release:		4
License:		GPL-2.0-or-later
URL:			https://fcitx-im.org/wiki/Fcitx
Source0:		https://download.fcitx-im.org/fcitx/%{name}-%{version}_dict.tar.xz
Source1:		xinput-%{name}
BuildRequires:		gcc-c++, pango-devel, dbus-devel, opencc-devel, wget, intltool, chrpath, opencc, cmake, libtool, doxygen
BuildRequires:		qt4-devel, gtk3-devel, gtk2-devel, libicu, xorg-x11-proto-devel, xorg-x11-xtrans-devel, gobject-introspection-devel, libxkbfile-devel, enchant-devel, iso-codes-devel, libicu-devel
BuildRequires:		libX11-devel, dbus-glib-devel, dbus-x11, desktop-file-utils, libxml2-devel, lua-devel, extra-cmake-modules, xkeyboard-config-devel, libuuid-devel, json-c-devel
Requires:		imsettings, imsettings-gnome, hicolor-icon-theme, dbus
Requires:		%{name}-libs%{?_isa} = %{version}-%{release}
Requires(post):		%{_sbindir}/alternatives
Requires(postun):	%{_sbindir}/alternatives

Patch1:			0001-fix-fcitx-desktop-hide-fcitx-from-kiran-menu-of-Kira.patch
Patch2:                 0001-fix-nodisplay.patch

%description
Fcitx is an input method framework with extension support. Currently it
supports Linux and Unix systems like FreeBSD.

Fcitx tries to provide a native feeling under all desktop as well as a light
weight core. You can easily customize it to fit your requirements.

%package libs
Summary:		Shared libraries for Fcitx
Provides:		%{name}-keyboard = %{version}-%{release}
Obsoletes:		%{name}-keyboard =< 4.2.3

%description libs
The %{name}-libs package provides shared libraries for Fcitx

%package devel
Summary:		Development files for Fcitx
Requires:		%{name}-libs%{?_isa} = %{version}-%{release}
Requires:		libX11-devel%{?_isa}

%description devel
The %{name}-devel package contains libraries and header files necessary for
developing programs using Fcitx libraries.


%prep
%autosetup -p1 -n %{name}-%{version}

%build
%cmake -DENABLE_GTK3_IM_MODULE=On -DENABLE_QT_IM_MODULE=On -DENABLE_OPENCC=On -DENABLE_LUA=On -DENABLE_GIR=On -DENABLE_XDGAUTOSTART=Off
%cmake_build

%install
%cmake_install

install -pm 644 -D %{SOURCE1} %{buildroot}%{_xinputconf}

# patch fcitx4-config to use pkg-config to solve libdir to avoid multiarch
# confilict
sed -i -e 's:%{_libdir}:`pkg-config --variable=libdir fcitx`:g' \
  %{buildroot}%{_bindir}/fcitx4-config

chmod +x %{buildroot}%{_datadir}/%{name}/data/env_setup.sh

%find_lang %{name}

desktop-file-install --delete-original \
  --dir %{buildroot}%{_datadir}/applications \
  %{buildroot}%{_datadir}/applications/%{name}-skin-installer.desktop

desktop-file-install --delete-original \
  --dir %{buildroot}%{_datadir}/applications \
  %{buildroot}%{_datadir}/applications/%{name}-configtool.desktop

desktop-file-install --delete-original \
  --dir %{buildroot}%{_datadir}/applications \
  %{buildroot}%{_datadir}/applications/%{name}.desktop

%post 
%{_sbindir}/alternatives --install %{_sysconfdir}/X11/xinit/xinputrc xinputrc %{_xinputconf} 55 || :

%postun  
if [ "$1" = "0" ]; then
  %{_sbindir}/alternatives --remove xinputrc %{_xinputconf} || :
  # if alternative was set to manual, reset to auto
  [ -L %{_sysconfdir}/alternatives/xinputrc -a "`readlink %{_sysconfdir}/alternatives/xinputrc`" = "%{_xinputconf}" ] && %{_sbindir}/alternatives --auto xinputrc || :
fi

%files -f %{name}.lang
%doc AUTHORS ChangeLog THANKS TODO
%license COPYING
%{_bindir}/createPYMB
%{_bindir}/fcitx
%{_bindir}/fcitx-*
%{_bindir}/mb2org
%{_bindir}/mb2txt
%{_bindir}/readPYBase
%{_bindir}/readPYMB
%{_bindir}/scel2org
%{_bindir}/txt2mb
%config %{_xinputconf}
%{_datadir}/applications/%{name}-configtool.desktop
%{_datadir}/applications/%{name}.desktop
%{_datadir}/applications/%{name}-skin-installer.desktop
%{_datadir}/dbus-1/services/org.fcitx.Fcitx.service
%{_datadir}/icons/hicolor/128x128/apps/*.png
%{_datadir}/icons/hicolor/16x16/apps/*.png
%{_datadir}/icons/hicolor/22x22/apps/*.png
%{_datadir}/icons/hicolor/24x24/apps/*.png
%{_datadir}/icons/hicolor/32x32/apps/*.png
%{_datadir}/icons/hicolor/48x48/apps/*.png
%{_datadir}/icons/hicolor/scalable/apps/*.svg
%{_datadir}/mime/packages/x-fskin.xml
%{_datadir}/%{name}/addon/fcitx-pinyin.conf
%{_datadir}/%{name}/addon/fcitx-pinyin-enhance.conf
%{_datadir}/%{name}/addon/fcitx-qw.conf
%{_datadir}/%{name}/addon/fcitx-table.conf
%{_datadir}/%{name}/addon/%{name}-[!pqt]*.conf
%{_datadir}/%{name}/addon/%{name}-punc.conf
%{_datadir}/%{name}/addon/%{name}-quickphrase.conf
%{_datadir}/%{name}/configdesc/fcitx-[!p]*.desc
%{_datadir}/%{name}/configdesc/fcitx-pinyin.desc
%{_datadir}/%{name}/configdesc/fcitx-pinyin-enhance.desc
%{_datadir}/%{name}/configdesc/[!ft]*.desc
%{_datadir}/%{name}/configdesc/table.desc
%{_datadir}/%{name}/data/
%{_datadir}/%{name}/dbus/daemon.conf
%{_datadir}/%{name}/imicon/pinyin.png
%{_datadir}/%{name}/imicon/[!ps]*.png
%{_datadir}/%{name}/imicon/shuangpin.png
%{_datadir}/%{name}/inputmethod/pinyin.conf
%{_datadir}/%{name}/inputmethod/qw.conf
%{_datadir}/%{name}/inputmethod/shuangpin.conf
%{_datadir}/%{name}/pinyin/
%{_datadir}/%{name}/py-enhance/
%{_datadir}/%{name}/skin/
%{_datadir}/%{name}/spell/
%{_datadir}/%{name}/table/*
%dir %{_datadir}/%{name}
%dir %{_datadir}/%{name}/addon
%dir %{_datadir}/%{name}/configdesc/
%dir %{_datadir}/%{name}/dbus/
%dir %{_datadir}/%{name}/imicon/
%dir %{_datadir}/%{name}/inputmethod/
%dir %{_datadir}/%{name}/table/
%{_mandir}/man1/createPYMB.1*
%{_mandir}/man1/fcitx.1*
%{_mandir}/man1/fcitx-remote.1*
%{_mandir}/man1/mb2org.1*
%{_mandir}/man1/mb2txt.1*
%{_mandir}/man1/readPYBase.1*
%{_mandir}/man1/readPYMB.1*
%{_mandir}/man1/scel2org.1*
%{_mandir}/man1/txt2mb.1*


%files libs
%license COPYING
%dir %{_libdir}/girepository-1.0/
%dir %{_libdir}/%{name}/
%{_libdir}/girepository-1.0/Fcitx-1.0.typelib
%{_libdir}/gtk-2.0/%{gtk2_binary_version}/immodules/im-fcitx.so
%{_libdir}/gtk-3.0/%{gtk3_binary_version}/immodules/im-fcitx.so
%{_libdir}/libfcitx*.so.*
%{_libdir}/%{name}/libexec/
%{_libdir}/%{name}/%{name}-pinyin-enhance.so
%{_libdir}/%{name}/%{name}-pinyin.so
%{_libdir}/%{name}/%{name}-[!pqt]*.so
%{_libdir}/%{name}/%{name}-punc.so
%{_libdir}/%{name}/%{name}-quickphrase.so
%{_libdir}/%{name}/%{name}-qw.so
%{_libdir}/%{name}/%{name}-table.so
%{_libdir}/qt4/plugins/inputmethods/qtim-fcitx.so


%files devel
%license COPYING
%{_bindir}/fcitx4-config
%{_libdir}/libfcitx*.so
%{_libdir}/pkgconfig/fcitx*.pc
%{_includedir}/fcitx*
%{_datadir}/cmake/%{name}/
%{_docdir}/%{name}/*
%dir %{_datadir}/gir-1.0
%{_datadir}/gir-1.0/Fcitx-1.0.gir

%changelog
* Wed Nov 20 2024 Funda Wang <fundawang@yeah.net> - 4.2.9.7-4
- adopt to new cmake macro

* Thu Dec 21 2023 liuzhilin <liuzhilin@uniontech.com> - 4.2.9.7-3
- hide fcitx from DDE

* Wed Jul 27 2022 yuanxing <yuanxing@kylinsec.com.cn> - 4.2.9.7-2
- hide fcitx from kiran-menu of kiran-desktop

* Tue Dec 22 2020 weidong <weidong@uniontech.com> - 4.2.9.7-1
- Initial Package
